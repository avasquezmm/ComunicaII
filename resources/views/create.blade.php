@extends('layouts.web')

@section('title')
    <section id="page-title" class="page-title-parallax page-title-dark page-title-center" style="background-image: url('{{ asset('images/sections/food-menu.jpg') }}'); background-size: cover; padding: 100px 0 120px;" data-bottom-top="background-position:0 0px;" data-top-bottom="background-position:0px -300px;">

        <div class="container clearfix">
            <h1 class="font-secondary capitalize ls0" style="font-size: 74px;">Create Order</h1>
        </div>

    </section><!-- #page-title end -->
@endsection
@section('content')
    <div class="content-wrap nobottompadding">

        <div class="container">
            @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @endif
            <order-alert user_id="{{ auth()->user()->id }}"></order-alert>
            <form method="post" action="{{ route('user.orders.store') }}" class="mb-0 row">
                {{ csrf_field() }}

                <div class="col-sm-6 mb-3">
                    <input type="text" id="template-contactform-name" name="address" value="" class="sm-form-control border-form-control required" placeholder="Address" />
                </div>

                <div class="col-6 mb-3">
                    <div><label> <input type="radio" checked="" value="medium" id="medium" name="size"> Medium </label></div>
                    <div><label> <input type="radio" value="large" id="large" name="size"> Large </label></div>
                    <div><label> <input type="radio" value="extra-large" id="extra-large" name="size"> Extra Large </label></div>
                </div>

                <div class="clear"></div>

                <div class="col-6 mb-3">
                    <label class="col-sm-2 control-label">Toppings</label>
                    <div class="col-sm-10">
                        <label class="checkbox-inline">
                            <input type="checkbox" name="toppings[]" value="pepperoni" id="pepperoni"> Pepperoni
                        </label>
                        <label class="checkbox-inline">
                            <input type="checkbox" name="toppings[]" value="extra-cheese" id="extra-cheese"> Extra Cheese
                        </label>
                        <label class="checkbox-inline">
                            <input type="checkbox" name="toppings[]" value="mushrooms" id="mushrooms"> Mushrooms
                        </label>
                        <label class="checkbox-inline">
                            <input type="checkbox" name="toppings[]" value="ground-beef" id="ground-beef"> Ground Beef
                        </label>
                        <label class="checkbox-inline">
                            <input type="checkbox" name="toppings[]" value="pineapple" id="inlineCheckbox3"> Pineapple
                        </label>
                    </div>
                </div>
                <div class="hr-line-dashed"></div>

                <div class="col-sm-6 mb-3">
                    <input type="text" id="template-contactform-name" name="instructions" value="" class="sm-form-control border-form-control required" placeholder="Special Instructions here" />
                </div>


                <div class="clear"></div>

                <div class="col-12 nobottommargin">
                    <button class="button button-circle button-large text-white ml-0 mt-3" type="submit" id="template-contactform-submit" name="template-contactform-submit" value="submit">Order Now</button>
                </div>
            </form>
    </div>
@endsection
