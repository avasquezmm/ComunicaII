@extends('layouts.web')

@section('slider')
    <section id="slider" class="slider-element swiper_wrapper slider-parallax force-full-screen full-screen clearfix" data-effect="fade" data-loop="true" data-autoplay="6000" data-speed="1400">

        <div class="slider-parallax-inner">
            <div class="swiper-container swiper-parent">
                <div class="swiper-wrapper">
                    <div class="swiper-slide" style="background-image: url('{{ asset('demos/restaurant/images/slider/1.jpg') }}'); background-position: center center;">
                        <div class="container dark clearfix">
                            <div class="slider-caption">
                                <h2 class="font-secondary ls0 t400 nott">Taste from Italy.</h2>
                                <p class="d-none d-sm-block font-primary">Who Needs a Boyfriend if there Pizza And WiFi are both available.</p>
                                <div class="static-content" style="position: relative; display: flex; justify-content: flex-start; flex-direction: row; margin-top: 30px">
                                    <img src="{{ asset('demos/restaurant/images/icons/bowl-white.svg') }}" width="42" height="42" alt="">
                                    <img class="leftmargin-sm" src="{{ asset('demos/restaurant/images/icons/spoon-white.svg') }}" width="42" height="42" alt="">
                                    <img class="leftmargin-sm" src="{{ asset('demos/restaurant/images/icons/glass-white.svg') }}" width="42" height="42" alt="">
                                    <img class="leftmargin-sm" src="{{ asset('demos/restaurant/images/icons/wifi-white.svg') }}" width="42" height="42" alt="">
                                </div>
                                <div class="static-content" style="position: relative; display: flex; justify-content: flex-start; flex-direction: row; margin-top: 30px">
                                    <a class="button button-circle button-large text-white ml-0 mt-3" href="{{route('user.orders.create')}}">Order Now</a>
                                </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </section>
@endsection

@section('content')
    <div style="position: absolute; top: 0; left: 0; width: 100%; z-index: 3; background: url('{{ asset('demos/restaurant/images/sketch.png') }}') repeat center bottom; background-size: auto 100%; height: 40px; margin-top: -40px;"></div>

    <div class="content-wrap">

        <div class="container section-contact topmargin-lg clearfix">
            <div class="row clearfix">

                <div class="col-lg-3 col-md-6 bottommargin-sm center">
                    <i class="i-plain i-xlarge divcenter nobottommargin icon-et-map"></i>
                    <h3 class="uppercase font-body" style="font-size: 22px; font-weight: 700;margin-top: 20px">Contact</h3>
                    <span class="font-primary">795 Folsom Ave, Suite 600<br>San Francisco, CA 94107</span>
                </div>

                <div class="col-lg-3 col-md-6 bottommargin-sm center">
                    <i class="i-plain i-xlarge divcenter nobottommargin icon-et-clock"></i>
                    <h3 class="uppercase font-body" style="font-size: 22px; font-weight: 700;margin-top: 20px">Opening Time</h3>
                    <span class="font-primary">Sun - Thu | 07:00 - 23:00 Hours<br>
							Fri - Sat | 08:00 - 01:00 Hours</span>
                </div>

                <div class="col-lg-3 col-md-6 bottommargin-sm center">
                    <i class="i-plain i-xlarge divcenter nobottommargin icon-et-clipboard"></i>
                    <h3 class="uppercase font-body" style="font-size: 22px; font-weight: 700;margin-top: 20px">Reservation</h3>
                    <span class="font-primary"><strong>Mobile: </strong>+62-111-222-333<br>
							<strong>Fax: </strong>(+62)-11-4752-1433</span>
                </div>

                <div class="col-lg-3 col-md-6 bottommargin-sm center">
                    <i class="i-plain i-xlarge divcenter nobottommargin icon-et-heart"></i>
                    <h3 class="uppercase font-body" style="font-size: 22px; font-weight: 700;margin-top: 20px">Social Contact</h3>
                    <div style="display: flex; justify-content: center">
                        <a href="#" class="social-icon si-borderless si-facebook">
                            <i class="icon-line2-social-facebook"></i>
                            <i class="icon-line2-social-facebook"></i>
                        </a>
                        <a href="#" class="social-icon si-borderless si-twitter">
                            <i class="icon-line2-social-twitter"></i>
                            <i class="icon-line2-social-twitter"></i>
                        </a>
                        <a href="#" class="social-icon si-borderless si-youtube">
                            <i class="icon-line2-social-youtube"></i>
                            <i class="icon-line2-social-youtube"></i>
                        </a>
                        <a href="#" class="social-icon si-borderless si-email3">
                            <i class="icon-line2-envelope"></i>
                            <i class="icon-line2-envelope"></i>
                        </a>
                    </div>
                </div>

            </div>
        </div>

    </div>

    <div style="position: absolute; bottom: 0; left: 0; width: 100%; z-index: 3; background: url('demos/restaurant/images/sketch-header.png') repeat center bottom; background-size: auto 100%; height: 40px; margin-bottom: -10px;"></div>
@endsection