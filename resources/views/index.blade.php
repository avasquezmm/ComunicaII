@extends('layouts.web')

@section('title')
    <section id="page-title" class="page-title-parallax page-title-dark page-title-center" style="background-image: url('{{ asset('images/sections/food-menu.jpg') }}'); background-size: cover; padding: 100px 0 120px;" data-bottom-top="background-position:0 0px;" data-top-bottom="background-position:0px -300px;">

        <div class="container clearfix">
            <h1 class="font-secondary capitalize ls0" style="font-size: 74px;">My Orders</h1>
        </div>

    </section><!-- #page-title end -->
@endsection
@section('content')
    <div class="content-wrap nobottompadding">

        <div class="container">
            @if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
            @endif
            <div class="panel-body">
                @if (session('message'))
                    <div class="alert alert-success">
                        {{ session('message') }}
                    </div>
                @endif

            @if ($orders->count() == 0)
                <p>No orders yet.</p>
                <a class="btn btn-success" href="{{ route('user.orders.create') }}">Order Pizza</a>

            @else

                <order-alert user_id="{{ auth()->user()->id }}"></order-alert>

                <div class="table-responsive">
                    <table class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Address</th>
                                <th>Size</th>
                                <th>Toppings</th>
                                <th>Instructions</th>
                                <th>Status</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($orders as $order)
                                <tr>
                                    <td>{{ $order->id }}</td>
                                    <td>{{ $order->address }}</td>
                                    <td>{{ $order->size }}</td>
                                    <td>{{ $order->toppings }}</td>
                                    <td>{{ $order->instructions }}</td>
                                    <td><a href="{{ route('user.orders.show', $order) }}">{{ $order->status->name }}</a></td>
                                </tr>
                            @endforeach
                        </tbody>

                    </table>
                </div> <!-- end table-responsive -->

            @endif

            </div>
        </div>
    </div>
</div>
@endsection
